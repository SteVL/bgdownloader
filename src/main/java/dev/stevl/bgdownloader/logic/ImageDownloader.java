package dev.stevl.bgdownloader.logic;

import java.io.File;
import java.net.URL;

/**
 * Описывает требования к загрузчику изображения
 *
 * @author Vladislav Stebenev
 */
public interface ImageDownloader {
        
    /**
     *
     * @param url
     * @param fileNameForSave
     * @param path
     * @throws java.lang.Exception
     * @throws IOException
     */
    void downloadImage(final URL url, final String fileNameForSave, final File path) throws Exception;
}
