package dev.stevl.bgdownloader.logic;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vladislav Stebenev
 */
public class SomeImageDownloader implements ImageDownloader {

    private static final Logger LOG = LoggerFactory.getLogger(SomeImageDownloader.class);

    /**
     *
     * @param url
     * @param fileNameForSave
     * @param path
     * @throws IOException
     */
    @Override
    public void downloadImage(URL url, String fileNameForSave, File path) throws IOException {
            URLConnection con = url.openConnection();
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
            File file = new File(path,fileNameForSave);
            FileOutputStream fos = new FileOutputStream(file);
            int ch;
            LOG.info("Имею ссылку для скачивания: {}", url.toString());
            LOG.info("Начинаю скачивать изображение...");
            while ((ch = bis.read()) != -1) {
                fos.write(ch);
            }
            LOG.info("Сохраняю в {}", path);
            bis.close();
            fos.flush();
            fos.close();
            LOG.info("Изображение загружено");
        
    }

}
