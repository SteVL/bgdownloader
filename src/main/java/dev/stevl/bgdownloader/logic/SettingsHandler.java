package dev.stevl.bgdownloader.logic;

/**
 *
 * @author Vladislav Stebenev
 */
public interface SettingsHandler {

    String getImageLink();

    String getImageFileName();

    String getPathToSave();

}
