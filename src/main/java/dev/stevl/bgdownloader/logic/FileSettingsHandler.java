package dev.stevl.bgdownloader.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vladislav Stebenev
 */
public class FileSettingsHandler implements SettingsHandler {

    private static final Logger LOG = LoggerFactory.getLogger(FileSettingsHandler.class);

    String file_image_name;
    String remote_file_link;
    String path_to_save;

    public FileSettingsHandler() throws Exception {
        readSettingsFromTxtFile();
    }

    @Override
    public String getImageLink() {
        LOG.debug(remote_file_link);
        return remote_file_link;
    }

    @Override
    public String getImageFileName() {
        LOG.debug(file_image_name);
        return file_image_name;
    }

    @Override
    public String getPathToSave() {
        LOG.debug(path_to_save);
        return path_to_save;
    }

    /**
     * Читает настройки из текстового файла
     * <p>
     * Settings structure:<br>
     * 1.Remote link;<br>
     * 2."Save as" image name<br>
     * 3.Path to save</p>
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void readSettingsFromTxtFile() throws Exception {
        LOG.info("Считываю параметры из файла settings.txt");
        BufferedReader buf = new BufferedReader(new FileReader("settings.txt"));
        String[] tempArr = new String[3];
        int cnt = 0;
        String line;
        while ((line = buf.readLine()) != null) {
            if (!line.startsWith("#")) {
                tempArr[cnt] = line;
//                LOG.debug(line);
                cnt++;
            }
        }
        remote_file_link = tempArr[0];
        file_image_name = tempArr[1];
        path_to_save = tempArr[2];
    }
}
