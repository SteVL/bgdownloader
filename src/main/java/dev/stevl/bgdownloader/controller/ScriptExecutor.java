package dev.stevl.bgdownloader.controller;

import dev.stevl.bgdownloader.logic.FileSettingsHandler;
import dev.stevl.bgdownloader.logic.ImageDownloader;
import dev.stevl.bgdownloader.logic.SettingsHandler;
import dev.stevl.bgdownloader.logic.SomeImageDownloader;
import java.io.File;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Vladislav Stebenev
 */
public class ScriptExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(ScriptExecutor.class);

    public void execute() throws Exception {
        LOG.info("Привет! Я скачаю за тебя изображение фона для Kosmos");
        LOG.info("Начинаю выполнение скрипта...");
        ImageDownloader downloader = new SomeImageDownloader();
        SettingsHandler settings = new FileSettingsHandler();
        URL url = new URL(settings.getImageLink());
        downloader.downloadImage(url, settings.getImageFileName(), new File(settings.getPathToSave()));
        LOG.info("Выполнение скрипта завершено.");
    }
}
