package dev.stevl.bgdownloader;

import dev.stevl.bgdownloader.view.ConsoleUserView;

/**
 *
 * @author Vladislav Stebenev
 */
public class Main {

    public static void main(String[] args) {
        new ConsoleUserView().startScript();
    }
}
