package dev.stevl.bgdownloader.view;

/**
 *
 * @author Vladislav Stebenev
 */
public interface UserView {
    
    String getStatusMsg();
    
   void startScript();
}
