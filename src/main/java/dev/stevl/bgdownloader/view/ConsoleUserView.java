package dev.stevl.bgdownloader.view;

import dev.stevl.bgdownloader.controller.ScriptExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vladislav Stebenev
 */
public class ConsoleUserView implements UserView{

    @Override
    public String getStatusMsg() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void startScript(){
        try { 
            new ScriptExecutor().execute();
        } catch (Exception ex) {
            Logger.getLogger(ConsoleUserView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
